/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 * Based on Jet-Set Willy, v1.0.1 by <Florent.Guillaume@ens.fr>
 * Linux port and preliminary sound by jmd@dcs.ed.ac.uk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module audio;

import core.atomic;
import core.thread;

import arsd.simpleaudio;

import engine;


// ////////////////////////////////////////////////////////////////////////// //
//version = debug_sound;
//__gshared AudioOutput audioOut;
private shared bool gameSoundOn = false;


// ////////////////////////////////////////////////////////////////////////// //
private:
__gshared const(ubyte)[] gsCurrent = null;
shared bool gsQuitThread = false;


// ////////////////////////////////////////////////////////////////////////// //
public void jssSetupSound () {
  if (cas(&gameSoundOn, false, true)) {
    new Thread(&audioPlayThread).start();
  }
}


public void jssAbortSound () {
  if (!atomicLoad(gameSoundOn)) return;
  atomicStore(gsQuitThread, true);
  while (atomicLoad(gameSoundOn)) {
    import core.time;
    Thread.sleep(50.msecs);
  }
}


public void jssStopAll () {
  synchronized(JSSLock.classinfo) {
    gsCurrent = null;
  }
}


public void jssPlaySfx (const(char)[] lump) {
  if (!atomicLoad(gameSoundOn)) return;
  if (auto lmp = lump in gameLumps) {
    if ((*lmp).length >= 32) {
      synchronized(JSSLock.classinfo) gsCurrent = *lmp;
      version(debug_sound) { import core.stdc.stdio; printf("PLAYING '%*s'\n", cast(uint)lump.length, lump.ptr); }
    }
  }
}


public void jssStopSfx () {
  if (!atomicLoad(gameSoundOn)) return;
  synchronized(JSSLock.classinfo) gsCurrent = null;
}


// ////////////////////////////////////////////////////////////////////////// //
private:
class JSSLock {}


// player thread
void audioPlayThread () {
  auto audioOut = AudioOutput(0);
  audioOut.fillData = delegate (short[] dest) {
    //version(debug_sound) { import core.stdc.stdio; printf("FILL: dest.length=%u; gsCurrent.length=%u\n", cast(uint)dest.length, cast(uint)gsCurrent.length); }
    if (atomicLoad(gsQuitThread)) {
      version(debug_sound) { import core.stdc.stdio; printf("AUDIO: stop\n"); }
      dest[] = 0;
      audioOut.stop();
      return;
    }
    synchronized(JSSLock.classinfo) {
      while (gsCurrent.length >= 2 && dest.length > 0) {
        import core.stdc.string : memcpy;
        memcpy(dest.ptr, gsCurrent.ptr, 2);
        gsCurrent = gsCurrent[2..$];
        dest = dest[1..$];
      }
      if (gsCurrent.length < 2) gsCurrent = null;
    }
    if (dest.length) dest[] = 0;
  };

  version(debug_sound) { import core.stdc.stdio; printf("audio thread started...\n"); }
  audioOut.play();
  version(debug_sound) { import core.stdc.stdio; printf("audio thread complete...\n"); }
  atomicStore(gameSoundOn, false);
}
