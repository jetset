/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 * Based on Jet-Set Willy, v1.0.1 by <Florent.Guillaume@ens.fr>
 * Linux port and preliminary sound by jmd@dcs.ed.ac.uk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module willy;

import x11gfx;
import engine;


// ////////////////////////////////////////////////////////////////////////// //
enum GameState {
  Title,
  Playing,
  Dead,
  Rocket,
  Teleport,
  Boat,
}


// ////////////////////////////////////////////////////////////////////////// //
static struct WillySavePos {
  int x, y, dir;
  int roomnum = -1;

  @property bool valid () const pure { return (roomnum >= 0); }
}


// ////////////////////////////////////////////////////////////////////////// //
static struct Willy {
  @disable this (this); // no copies, please

  GameState gstate = GameState.Title;

  WillySavePos saveposPrevRoom;
  WillySavePos savepos;

  int x, y;
  int dir; // -1: left; 1: right
  int mRoomnum;
  int y_correction;

  bool close_to_lift;
  bool dead;
  int height_fallen;
  bool jumping;
  int x_delta;
  int y_delta;
  int onwhat;
  int stairtype;

  int exitDir = -1; // exit direction

  int toleft;
  int toright;
  int tojump;

  // keys
  bool goright;
  bool goleft;
  bool dojump;

  bool ropeDontCatch;
  int ropeCatchCounter;
  bool ropeCatched;
  int ropeCatchedPos;

  int invulnTimer; // >0: Willy is invulnerable (only for monsters)

  int rocket_cnt;
  int foot_cnt;

  enum {
    ON_WALL      = (1U<<0),
    ON_STAIR     = (1U<<1),
    ON_MOVELEFT  = (1U<<2),
    ON_MOVERIGHT = (1U<<3),
  }

  /*
  int nb_automoves;
  int x_automove;
  int y_automove;
  int dir_automove;
  int automove_offset = 0;
  */

  void newLife () {
    x_delta = 0;
    y_delta = 9;
    y_correction = 0;
    stairtype = 0;
    jumping = 0;
    height_fallen = 0;
    onwhat = 0;
    dead = false;
    exitDir = -1;
    ropeDontCatch = false;
    ropeCatched = false;
    ropeCatchCounter = 0;
    room.loadRoomGfx();
    room.specInit(this);
    room.resetMonsters();
    invulnTimer = 10; // 0.5 seconds
    toleft = toright = tojump = 0;
    goright = goleft = dojump = false;
  }

  // debug function
  void setXY (int ax, int ay) {
    x_delta = 0;
    y_delta = 9;
    y_correction = 0;
    stairtype = 0;
    jumping = 0;
    height_fallen = 0;
    onwhat = 0;
    dead = false;
    exitDir = -1;
    ropeDontCatch = false;
    ropeCatched = false;
    ropeCatchCounter = 0;
    x = (ax/2)&0x7f;
    y = (ay/8*8)&0x7f;
    toleft = toright = tojump = 0;
    goright = goleft = dojump = false;
    //rocket_cnt = 0;
    //foot_cnt = 0;
  }

  bool restoreSave (const ref WillySavePos svp) {
    if (!svp.valid) return false;
    x = svp.x;
    y = svp.y;
    dir = svp.dir;
    mRoomnum = svp.roomnum;
    newLife();
    return true;
  }

  bool restoreSavePos () { return restoreSave(savepos); }
  bool restoreSavePosPrev () { return restoreSave(saveposPrevRoom); }

  WillySavePos saveSave () {
    if (savepos.valid) return savepos;
    return WillySavePos();
  }

  @property int roomnum () { return mRoomnum; }
  @property JSWRoom room () { return gameRooms[mRoomnum]; }

  void gotoRoom (int ridx) {
    exitDir = -1;
    if (ridx < 0 || ridx >= gameRooms.length) return;
    if (gameRooms[ridx] is null) return;
    mRoomnum = ridx;
    room.loadRoomGfx();
    room.specInit(this);
    room.resetMonsters();
  }

  int forSprite (scope int delegate (const(ubyte)[] lmp, uint ofs, int scrx, int scry) dg) {
    if (dg is null) return 0;
    room.loadRoomGfx();
    auto lmp = room.willyLmp;
    auto sprbase = room.willySprIdx;
    // nightmare room: backwards
    //spridx = sprbase+(x&3)+4*(dir == (roomnum == 0x1c ? 1 : -1) ? 1 : 0);
    auto spridx = sprbase+(x&3)+4*(dir == (room.willyLump == "sprmons" && sprbase == 0x62 ? 1 : -1) ? 1 : 0);
    auto scrx = ((x&~3)<<1);
    auto scry = y+y_correction;
    return dg(lmp, spridx, scrx, scry);
  }

  void draw (bool debugShowCollision=false) {
    forSprite(delegate (const(ubyte)[] lmp, uint ofs, int scrx, int scry) {
      bool collided = (debugShowCollision ? checkSpriteCollision(lmp, ofs, scrx, scry) : false);

      // pre-check
      /*
      room.rope.forEachPoint(delegate (int pxnum, int x, int y) {
        if (pxnum >= Rope.NBP) return 0;
        if (!ropeCatched) {
          auto col = forSprite(delegate (const(ubyte)[] lmp, uint ofs, int scrx, int scry) {
            return (checkSpritePixelCollision(lmp, ofs, scrx, scry, x, y) ? 1 : 0);
          });
          if (col) {
            collided = true;
            return 1; // stop
          }
        }
        return 0;
      });
      */

      drawSprite(lmp, ofs, scrx, scry, (collided ? 5 : 3));
      return 0;
    });
  }

  bool checkMonsterCollision () {
    return
      forSprite(delegate (const(ubyte)[] lmp, uint ofs, int scrx, int scry) {
        return (checkSpriteCollision(lmp, ofs, scrx, scry) ? 1 : 0);
      }) != 0;
  }

  void delegate () onTakeItem;

  void takeItem (int x, int y) {
    if (room.takeAt(x, y)) {
      if (onTakeItem !is null) onTakeItem();
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  void processExit () {
    if (dead) return;

    if (exitDir >= 0) {
      if (room.exits[exitDir] >= 0) {
        switch (exitDir) {
          case JSWRoom.Exit.Up:
            y = 104;
            if (close_to_lift || jumping) x_delta = 0;
            goto endupdown;
          case JSWRoom.Exit.Down:
            y = 0;
            if ((height_fallen -= 8) < 0) height_fallen = 0;
           endupdown:
            x += x_delta;
            x_delta = 0;
            jumping = 0;
            y_delta = 9;
            break;
          case JSWRoom.Exit.Left:
            x = 120;
            break;
          case JSWRoom.Exit.Right:
            x = 3;
            break;
          default:
        }
        gotoRoom(room.exits[exitDir]);
      }
      y_correction = 0;
      exitDir = -1;
    }
  }

  void move () {
    int checkLifts () {
      int y_on_lift;

      this.close_to_lift = false;

      if (y_delta >= 0) {
        foreach (ref mon; this.room.monsters) {
          if (!mon.lift) continue;
          int dx = this.x-mon.x+4;
          if (dx < 0 || dx >= 12) continue;
          int liy = mon.y;
          int d = (mon.dy == 1 ? 1 : -1);
          if (mon.counter == 1) d = -d;
          liy += d;
          int dy = this.y+this.y_correction-liy+21;
          if (dy < 0 || dy >= 7) continue;
          this.close_to_lift = true;
          y_on_lift = liy-0x10;
        }
      }

      if (!this.close_to_lift) return 0;

      if (this.height_fallen >= 48) {
        this.dead = true;
        return 0;
      }
      this.close_to_lift = false;
      this.height_fallen = 0;
      if (this.tojump) return 1;
      this.y = y_on_lift&~7;
      this.y_correction = y_on_lift&7;
      this.jumping = false;
      this.y_delta = 9;
      this.onwhat = ON_WALL;
      this.close_to_lift = true;
      return 2;
    }

    // returns `true` if Willy catched the rope
    bool doRope () {
      if (!room.hasRope) return false;

      if (ropeCatchCounter > 0) --ropeCatchCounter;
      if (ropeDontCatch || ropeCatchCounter /*|| !ropeCatched*/) return false;

      // pre-check
      if (!ropeCatched) {
        room.rope.forEachPoint(delegate (int pxnum, int x, int y) {
          if (pxnum >= Rope.NBP) return 0;
          if (!ropeCatched) {
            auto col = forSprite(delegate (const(ubyte)[] lmp, uint ofs, int scrx, int scry) {
              return (checkSpritePixelCollision(lmp, ofs, scrx, scry, x, y) ? 1 : 0);
            });
            if (col) {
              ropeCatchedPos = pxnum;
              ropeCatched = true;
              return 1; // stop
            }
          }
          return 0;
        });

        if (!ropeCatched) return false;
      }

      // post-check
      room.rope.forEachPoint(delegate (int pxnum, int rx, int ry) {
        if (pxnum == 0) return 0;
        --pxnum;
        if (ropeCatched && pxnum == ropeCatchedPos) {
          if (mRoomnum == 0x1b || mRoomnum == 0x12 || mRoomnum == 0x6f) {
            if (ropeCatchedPos <= 0x0e) ropeCatchedPos = 0x0e;
          }
          jumping = false;
          height_fallen = 0;
          y_delta = 9;
          onwhat = ON_WALL;
          y = ry-8;
          if (y < 0) exitDir = JSWRoom.Exit.Up; /* XXX exit */
          x = rx/2-2;
          return 1; // stop
        }
        return 0;
      });

      int dpos = (toright != 0 ? 1 : 0)-(toleft != 0 ? 1 : 0);
      if (dpos != 0) x_delta = dpos;
      if (room.rope.ropeDir == room.rope.ropeSpeed) dpos = -dpos;
      ropeCatchedPos -= dpos;
      if (ropeCatchedPos != Rope.NBP) {
        if (!tojump) return true;
        jumping = true;
        y_delta = -8;
      }
      y &= ~7;
      ropeDontCatch = true;
      ropeCatchCounter = 7;
      ropeCatched = false;
      return false;
    }

    int old_x_pos;
    int old_y_pos;
    int nb;
    bool shunt;
    bool maybe_save_pos = false;
    int maybe_save_x;
    int maybe_save_y;
    int maybe_save_lastdir;
    int maybe_save_room;

    JSWRoom.Tile c, c1, c2;

    if (dead) {
      invulnTimer = 0;
      return;
    }
    if (--invulnTimer < 0) invulnTimer = 0;

    toleft = goleft;
    toright = goright;
    tojump = dojump;

    /+
    if (special_room) {
        switch (do_special ()) {
        case 0:
            break;
        case 3: /* teleport */
            return;
        default:
            printf ("bug\n");
            exit (2);
        }
    }
    +/

    switch (checkLifts()) {
      case 0: break;
      case 1: goto aftertestjump;
      case 2: goto changedelta;
      default: assert(0, "wtf?!");
    }

    close_to_lift = false;

    // inside "move_player" because has to override some movements
    shunt = doRope();
    if (exitDir >= 0) return;
    if (shunt) goto aftershunt;

    if (tojump && !jumping && onwhat) {
      c1 = room.tileAt(x, y+16);
      c2 = room.tileAt(x+4, y+16);
      if (c1 != 0 || c2 != 0) {
        if (c1 != JSWRoom.Tile.ConvLeft && c1 != JSWRoom.Tile.ConvRight && c2 != JSWRoom.Tile.ConvLeft && c2 != JSWRoom.Tile.ConvRight) {
          x_delta = 0;
          if (toleft) --x_delta;
          if (toright) ++x_delta;
        }
       aftertestjump:
        y_delta = -8;
        jumping = 1;
        y_correction = 0;
        stairtype = 0;
      }
    }

    old_y_pos = y;
    y += (y_delta >> 1);

    if (y < 0) {
      exitDir = JSWRoom.Exit.Up;
      return;
    }

    if (y_delta >= 0 && (old_y_pos & 7) == 0) {
      stairtype = 0;
      y_correction = 0;
      onwhat = 0;
      for (int i = 0; i <= 4; i += 4) {
        c = room.tileAt(x+i, y+16);
        switch (c) {
          case JSWRoom.Tile.Crumb:
          case JSWRoom.Tile.Wall:
            onwhat |= ON_WALL;
            maybe_save_pos = true;
            maybe_save_x = x;
            maybe_save_y = y&~7;
            maybe_save_lastdir = dir;
            maybe_save_room = mRoomnum;
            break;
          case JSWRoom.Tile.Deadly:
            dead = true;
            return;
          case JSWRoom.Tile.StairRight:
          case JSWRoom.Tile.StairLeft:
            onwhat |= ON_STAIR;
            stairtype = c;
            break;
          case JSWRoom.Tile.ConvLeft:
            onwhat |= ON_MOVELEFT;
            break;
          case JSWRoom.Tile.ConvRight:
            onwhat |= ON_MOVERIGHT;
            break;
          case JSWRoom.Tile.Item:
            break;
          default:
        }
      }

      if (onwhat&(ON_MOVELEFT|ON_MOVERIGHT)) {
        int ndir = (onwhat&ON_MOVELEFT ? -1 : 1);
        if (x_delta == 0) {
          if (ndir == -1) toleft = 1; else toright = 1;
        } else {
          if (ndir == dir || ((toleft|toright) == 0) || (ndir == -1 ? toleft : toright)) {
            if (ndir == -1) {
              toleft = 1;
              toright = 0;
            } else {
              toleft = 0;
              toright = 1;
            }
          }
        }
      }

      if (onwhat == 0) {
        if (y_delta == 9) {
          ropeDontCatch = false;
          x_delta = 0;
          height_fallen += 8;
          if (height_fallen >= 0x96) height_fallen = 0x82; /* XXX ??? */
        }
      } else {
        if (height_fallen+(jumping ? 0x14 : 0) >= 0x28) {
          dead = true;
          return;
        }
        ropeDontCatch = false;
        y = old_y_pos;
        jumping = 0;
        height_fallen = 0;
        y_delta = 9;
      }
    }

    if (!jumping && onwhat) {
     changedelta:
      x_delta = 0;
      if (toleft) --x_delta;
      if (toright) ++x_delta;
    }

    old_x_pos = x;
    x += x_delta;

    if (!close_to_lift) {
      if (!jumping) {
        c = room.tileAt(x, y+8);
        if (c == JSWRoom.Tile.StairLeft && (x&3) == 3 && x_delta == -1) {
          y -= 8;
          stairtype = c;
          onwhat = ON_WALL;
        }
        c = room.tileAt(x+4, y+8);
        if (c == JSWRoom.Tile.StairRight && (x&3) == 0 && x_delta == 1) {
          y -= 8;
          stairtype = c;
          onwhat = ON_WALL;
        }
        c = room.tileAt(x-4, y+16);
        if (c == JSWRoom.Tile.StairLeft && (x&3) == 0 && x_delta == 1) {
          y += 8;
          stairtype = c;
          onwhat = ON_WALL;
        }
        c = room.tileAt(x+8, y+16);
        if (c == JSWRoom.Tile.StairRight && (x&3) == 3 && x_delta == -1) {
          y += 8;
          stairtype = c;
          onwhat = ON_WALL;
        }

        // test if at top of stairs
        if (stairtype == JSWRoom.Tile.StairRight && room.tileAt(x+4, y+16) != JSWRoom.Tile.StairRight) {
          stairtype = 0;
        } else if (stairtype == JSWRoom.Tile.StairLeft && room.tileAt(x, y+16) != JSWRoom.Tile.StairLeft) {
          stairtype = 0;
        }
      }
      switch (stairtype) {
        case JSWRoom.Tile.StairLeft:
          y_correction = 2*(x&3);
          break;
        case JSWRoom.Tile.StairRight:
          y_correction = 2*(3-(x&3));
          break;
        default:
          y_correction = 0;
          break;
      }
    }

   aftershunt:
    nb = (y&7 ? 3 : 2);
    for (int j = 0; j < nb; ++j) {
      for (int i = 0; i <= 4; i += 4) {
        c = room.tileAt(x+i, y+8*j);
        if (c == JSWRoom.Tile.Deadly) {
          dead = true;
          return;
        }
        if (c == JSWRoom.Tile.Item) {
          takeItem(x+i, y+8*j);
        } else if (c == JSWRoom.Tile.Wall) {
          x = old_x_pos;
          j = nb;
          break;
        }
      }
    }

    if (jumping && y_delta < 0 && (old_y_pos&7) == 0) {
      for (int i = 0; i <= 4; i += 4) {
        c = room.tileAt(x+i, y);
        if (c == JSWRoom.Tile.Deadly) {
          dead = true;
          return;
        }
        if (c == JSWRoom.Tile.Item) {
          takeItem(x+i, y);
        } else if (c == JSWRoom.Tile.Wall) {
          y = old_y_pos;
          y_delta = 7;
          break;
        }
      }
    }

    if (maybe_save_pos) {
      if (savepos.roomnum >= 0 && savepos.roomnum != maybe_save_room) {
        saveposPrevRoom = savepos;
      }
      savepos.x = maybe_save_x;
      savepos.y = maybe_save_y;
      savepos.dir = maybe_save_lastdir;
      savepos.roomnum = maybe_save_room;
    }

    if (++y_delta == 10) y_delta = 9;

    if (y+y_correction < 0) {
      exitDir = JSWRoom.Exit.Up;
      return;
    }

    if (y+y_correction >= 0x6d) {
      exitDir = JSWRoom.Exit.Down;
      return;
    }

    /* if (!onwhat || jumping) { dosound; } */

    if (x_delta != 0) dir = x_delta;

    if (x == 0) {
      exitDir = JSWRoom.Exit.Left;
      return;
    }

    if (x == 0x7b) {
      exitDir = JSWRoom.Exit.Right;
      return;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void restartGame (ref Willy willy) {
  foreach (JSWRoom room; gameRooms) room.resetRoom();
  willy.savepos.roomnum = -1;
  willy.saveposPrevRoom.roomnum = -1;
  willy.x = willyStart.x/2;
  willy.y = willyStart.y;
  willy.dir = willyStart.dir;
  willy.gotoRoom(willyStart.room);
  willy.newLife();
}
