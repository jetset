/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 * Based on Jet-Set Willy, v1.0.1 by <Florent.Guillaume@ens.fr>
 * Linux port and preliminary sound by jmd@dcs.ed.ac.uk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module jetset;

import arsd.simpledisplay;
import arsd.simpleaudio;

import wadarc;
import x11gfx;
import engine;
import willy;
import audio;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool debugHarmlessMonsters = false;
__gshared WillySavePos[10] gameSaves;
__gshared bool gamePaused = false;
__gshared int gvGameFlash = 0;
__gshared bool gameJustStarted = true;


// ////////////////////////////////////////////////////////////////////////// //
void jumpToRoom (ref Willy willy, int dir) {
  auto room = gameRooms[willy.roomnum];
  if (room.exits[dir] < 0) return;
  willy.gotoRoom(room.exits[dir]);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int gvTitleFadingOut;

void gsTitleEnter (ref Willy willy) {
  blitShine = 0;
  blitType = BlitType.Normal;
  gvTitleFadingOut = 0;
  jssPlaySfx("jss_titl");
}

void gsTitleExit (ref Willy willy) {
  gamePaused = false;
  gameJustStarted = true;
}

void gsTitleKeys (ref Willy willy, KeyEvent evt) {
  if (!evt.pressed) return;
  switch (evt.key) {
    case Key.Space: case Key.Enter:
      if (gvTitleFadingOut) {
        willy.restartGame();
        willy.switchState(GameState.Playing);
      } else {
        gvTitleFadingOut = 1;
      }
      break;
    default:
  }
}

void gsTitleStep (ref Willy willy) {
}

void gsTitleDraw (ref Willy willy) {
  //drawStr(4, vbufH-12, "items: %s".format(countAllItems()), rgbcol(255, 127, 0));
  drawStr!"d10"(10, 10, "Jet Set Willy II", rgbcol(255, 127, 0));
  drawStr!"dos"(10, 22, "Jet Set Willy II", rgbcol(255, 127, 0));
  drawStrProp!"dos"(10, 32, "Jet Set Willy II", rgbcol(255, 127, 0));
  drawStrProp(10, 42, "Jet Set Willy II", rgbcol(255, 127, 0));
  if (gvTitleFadingOut) {
    blitShine = -gvTitleFadingOut*10;
    if (++gvTitleFadingOut >= 20) {
      willy.restartGame();
      willy.switchState(GameState.Playing);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void gsGameKeysRestoreSaves (ref Willy willy, KeyEvent evt) {
  switch (evt.key) {
    case Key.R:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift) { willy.restartGame(); willy.switchState(GameState.Playing); break; }
        if (evt.modifierState&ModifierState.alt) { if (willy.restoreSavePos()) willy.switchState(GameState.Playing); break; }
      }
      break;
    case Key.P:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift) { willy.restartGame(); willy.switchState(GameState.Playing); break; }
        if (evt.modifierState&ModifierState.alt) { if (willy.restoreSavePosPrev()) willy.switchState(GameState.Playing); break; }
        gamePaused = !gamePaused;
      }
      break;
    case Key.N0: .. case Key.N9:
      if (evt.pressed) {
        // load save
        if (evt.modifierState&ModifierState.shift) {
          if (willy.restoreSave(gameSaves[evt.key-Key.N0])) willy.switchState(GameState.Playing);
          break;
        }
        // save save
        if (evt.modifierState&ModifierState.alt) {
          if (!willy.dead && willy.gstate != GameState.Title && willy.gstate != GameState.Dead) {
            gameSaves[evt.key-Key.N0] = willy.saveSave();
          }
          break;
        }
      }
      break;
    default:
  }
}


// shared by all states
void gsGameKeys (ref Willy willy, KeyEvent evt) {
  willy.gsGameKeysRestoreSaves(evt);
  switch (evt.key) {
    case Key.Left:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift) { willy.jumpToRoom(JSWRoom.Exit.Left); break; }
      }
      willy.goleft = (evt.pressed ? 1 : 0);
      break;
    case Key.Right:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift) { willy.jumpToRoom(JSWRoom.Exit.Right); break; }
      }
      willy.goright = (evt.pressed ? 1 : 0);
      break;
    case Key.Up:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift) { willy.jumpToRoom(JSWRoom.Exit.Up); break; }
      }
      willy.dojump = (evt.pressed ? 1 : 0);
      break;
    case Key.Down:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift) { willy.jumpToRoom(JSWRoom.Exit.Down); break; }
      }
      break;
    case Key.Ctrl:
      willy.dojump = (evt.pressed ? 1 : 0);
      break;
    case Key.D:
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.alt) { debugHarmlessMonsters = !debugHarmlessMonsters; break; }
      }
      break;
    case Key.Space: case Key.Enter:
      if (evt.pressed) {
        if (willy.dead) {
          if (!willy.restoreSavePos()) willy.restartGame();
          willy.switchState(GameState.Playing);
        }
      }
      break;
    case Key.W: // alt+shift+w: collect all items
      if (evt.pressed) {
        if (evt.modifierState&ModifierState.shift && evt.modifierState&ModifierState.alt) { JSWRoom.cheatCollectAllItems(); break; }
      }
      break;
    default: break;
  }
}


// shared by all states
void gsGameStep (ref Willy willy) {
  if (gamePaused) return;
  auto room = gameRooms[willy.roomnum];
  if (room is null) assert(0, "wtf?!");
  if (willy.dead) return;
  scope(exit) {
    if (willy.dead) willy.switchState(GameState.Dead); // willy just died, switch state
  }
  ++frameno; // let it move! (animate lifts, items, etc.)
  willy.room.stepSpecial(willy);
  willy.move();
  if (willy.dead) return; // willy just died
  room.step();
  // check collisions
  room.buildColMap(willy);
  // check monster collision here
  if (!debugHarmlessMonsters && !willy.dead && willy.invulnTimer == 0 && willy.checkMonsterCollision()) {
    willy.dead = true;
    return;
  }
  // if willy should exit to another room, do it here
  willy.processExit();
}


// shared by all states
void gsGameDraw (ref Willy willy) {
  auto room = gameRooms[willy.roomnum];
  if (room !is null) {
    room.drawTiles();
    room.drawMonsters(willy);
    willy.draw(debugHarmlessMonsters);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void gsPlayingEnter (ref Willy willy) {
  jssStopSfx();
  if (willy.dead) willy.newLife();
  gvGameFlash = 0;
  blitShine = 0;
  blitType = BlitType.Normal;
  if (gameJustStarted) {
    gameJustStarted = false;
    // special rooms
    //willy.gotoRoom(0x21); // bedroom
    //{ willy.gotoRoom(0x77); willy.setXY(120, 90); } // rocket
    //willy.gotoRoom(0x4b); // rigor mortis
    //willy.gotoRoom(0x4c); // crypt
    //willy.gotoRoom(0x3b); // hell
    //{ willy.gotoRoom(0x71); willy.setXY(10, 38); } // tribbles
    //willy.gotoRoom(0x7e); // eggoids
    //willy.gotoRoom(0x67); // foot
    //willy.gotoRoom(0x60); // beam down
    //willy.gotoRoom(0x7f); // beam up
    //{ willy.gotoRoom(0x50); willy.setXY(50, 88); } // isle
    //{ willy.gotoRoom(0x44); willy.setXY(50, 88); } // bell
    //willy.gotoRoom(0x79); // invasion
    //willy.gotoRoom(0x47); // trip switch
    //{ willy.gotoRoom(0x38); willy.setXY(10, 96); } // yacht
  }
}

void gsPlayingExit (ref Willy willy) {
}

void gsPlayingKeys (ref Willy willy, KeyEvent evt) {
  willy.gsGameKeys(evt);
}

void gsPlayingStep (ref Willy willy) {
  willy.gsGameStep();
  auto room = willy.room;
  switch (room.sptype) {
    case JSWRoom.SpType.Rocket:
      if (room.countItems == 0 && willy.y == 0x30) {
        willy.rocket_cnt = 0x80;
        willy.switchState(GameState.Rocket);
      }
      break;
    default:
  }
}

void gsPlayingDraw (ref Willy willy) {
  if (gvGameFlash > 0) {
    --gvGameFlash;
    blitShine = 100;
  } else {
    gvGameFlash = 0;
    blitShine = 0;
  }
  if (gamePaused) {
    blitType = BlitType.Green;
  } else {
    blitType = BlitType.Normal;
  }
  willy.gsGameDraw();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int gvDeathFadingOut;

void gsDeadEnter (ref Willy willy) {
  jssPlaySfx("jss_dead");
  blitShine = 0;
  blitType = BlitType.Red;
  gvDeathFadingOut = 0;
}

void gsDeadExit (ref Willy willy) {
}

void gsDeadKeys (ref Willy willy, KeyEvent evt) {
  switch (evt.key) {
    case Key.Space: case Key.Enter:
      if (evt.pressed) {
        if (!willy.restoreSavePos()) willy.restartGame();
        willy.switchState(GameState.Playing);
        return;
      }
      break;
    default:
  }
  willy.gsGameKeysRestoreSaves(evt);
}

void gsDeadStep (ref Willy willy) {
  //willy.gsGameStep();
  if (blitShine > -180) --blitShine;
}

void gsDeadDraw (ref Willy willy) {
  willy.gsGameDraw();
}


// ////////////////////////////////////////////////////////////////////////// //
void gsRocketEnter (ref Willy willy) {
}

void gsRocketExit (ref Willy willy) {
}

void gsRocketKeys (ref Willy willy, KeyEvent evt) {
  willy.gsGameKeys(evt);
}

void gsRocketStep (ref Willy willy) {
  willy.gsGameStep();
}

void gsRocketDraw (ref Willy willy) {
  willy.gsGameDraw();
}


// ////////////////////////////////////////////////////////////////////////// //
void gsTeleportEnter (ref Willy willy) {
}

void gsTeleportExit (ref Willy willy) {
}

void gsTeleportKey (ref Willy willy, KeyEvent evt) {
  willy.gsGameKeys(evt);
}

void gsTeleportStep (ref Willy willy) {
  willy.gsGameStep();
}

void gsTeleportDraw (ref Willy willy) {
  willy.gsGameDraw();
}


// ////////////////////////////////////////////////////////////////////////// //
void gsBoatEnter (ref Willy willy) {
}

void gsBoatExit (ref Willy willy) {
}

void gsBoatKey (ref Willy willy, KeyEvent evt) {
  willy.gsGameKeys(evt);
}

void gsBoatStep (ref Willy willy) {
  willy.gsGameStep();
}

void gsBoatDraw (ref Willy willy) {
  willy.gsGameDraw();
}


// ////////////////////////////////////////////////////////////////////////// //
// if forced, do leave/enter even if state wasn't changed
void switchState (ref Willy willy, GameState newstate, bool force=false) {
  if (!force && willy.gstate == newstate) return;
  int newdead = 0; // 0: don't change; <0: dead; >0: alive
  final switch (willy.gstate) {
    case GameState.Title: gsTitleExit(willy); break;
    case GameState.Playing: gsPlayingExit(willy); newdead = 1; break;
    case GameState.Dead: gsDeadExit(willy); newdead = -1; break;
    case GameState.Rocket: gsRocketExit(willy); newdead = 1; break;
    case GameState.Teleport: gsTeleportExit(willy); newdead = 1; break;
    case GameState.Boat: gsBoatExit(willy); newdead = 1; break;
  }
  willy.gstate = newstate;
  if (newdead) {
    if (willy.dead && newdead > 0) willy.newLife();
    if (!willy.dead && newdead < 0) willy.dead = true; //FIXME
  }
  final switch (willy.gstate) {
    case GameState.Title: gsTitleEnter(willy); break;
    case GameState.Playing: gsPlayingEnter(willy); break;
    case GameState.Dead: gsDeadEnter(willy); break;
    case GameState.Rocket: gsRocketEnter(willy); break;
    case GameState.Teleport: gsTeleportEnter(willy); break;
    case GameState.Boat: gsBoatEnter(willy); break;
  }
}


void stepState (ref Willy willy) {
  final switch (willy.gstate) {
    case GameState.Title: gsTitleStep(willy); break;
    case GameState.Playing: gsPlayingStep(willy); break;
    case GameState.Dead: gsDeadStep(willy); break;
    case GameState.Rocket: gsRocketStep(willy); break;
    case GameState.Teleport: gsTeleportStep(willy); break;
    case GameState.Boat: gsBoatStep(willy); break;
  }
}


void drawState (ref Willy willy) {
  final switch (willy.gstate) {
    case GameState.Title: gsTitleDraw(willy); break;
    case GameState.Playing: gsPlayingDraw(willy); break;
    case GameState.Dead: gsDeadDraw(willy); break;
    case GameState.Rocket: gsRocketDraw(willy); break;
    case GameState.Teleport: gsTeleportDraw(willy); break;
    case GameState.Boat: gsBoatDraw(willy); break;
  }
}


void keysState (ref Willy willy, KeyEvent evt) {
  final switch (willy.gstate) {
    case GameState.Title: gsTitleKeys(willy, evt); break;
    case GameState.Playing: gsPlayingKeys(willy, evt); break;
    case GameState.Dead: gsDeadKeys(willy, evt); break;
    case GameState.Rocket: gsRocketKeys(willy, evt); break;
    case GameState.Teleport: gsTeleportKey(willy, evt); break;
    case GameState.Boat: gsBoatKey(willy, evt); break;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  registerWad("data/jetset.wad");
  registerWad("data/music.wad", true);

  for (int idx = 1; idx < args.length; ++idx) {
    if (args[idx] == "-file") {
      if (args.length-idx < 2) assert(0, "arg?!");
      registerWad(args[idx+1]);
      ++idx;
    } else {
      assert(0, "invalid arg");
    }
  }

  loadGameData();

  jssSetupSound();
  scope(exit) jssAbortSound();

  auto sdwin = x11gfxInit("Jet Set Willy");
  {
    clear(0);
    realizeVBuf();
    x11gfxBlit();
  }

  bool doQuit;
  Willy willy;

  willy.onTakeItem = delegate () {
    gvGameFlash = 2;
    jssPlaySfx("jss_pick");
  };

  // hacked init
  willy.restartGame();
  willy.dead = true;

  willy.switchState(GameState.Title, true); // forced switch

  sdwin.eventLoop(1000/20,
    // timer
    delegate () {
      if (sdwin.closed) return;
      clear(0);
      willy.stepState();
      willy.drawState();
      {
        import std.format : format;
        drawStr(4, vbufH-12, "items: %s".format(countAllItems()), rgbcol(255, 127, 0));
      }
      realizeVBuf();
      x11gfxBlit();
    },
    // keyboard
    delegate (KeyEvent evt) {
      if (sdwin.closed) return;
      switch (evt.key) {
        case Key.Q:
          if (evt.pressed) {
            if (evt.modifierState&ModifierState.ctrl) { doQuit = true; break; }
            if (evt.modifierState&ModifierState.alt) { willy.switchState(GameState.Title); break; }
          }
          break;
        case Key.C:
          if (evt.pressed) {
            if (evt.modifierState&ModifierState.ctrl) { willy.switchState(GameState.Title); break; }
          }
          break;
        case Key.X:
          if (evt.pressed) {
            if (evt.modifierState&ModifierState.alt) { doQuit = true; break; }
          }
          break;
        case Key.Escape:
          //doQuit = true;
          break;
        default: break;
      }
      if (doQuit) { sdwin.close(); return; }
      willy.keysState(evt);
    },
    // mouse
    delegate (MouseEvent evt) {
      if (sdwin.closed) return;
      if (willy.gstate == GameState.Title) return;
      if (evt.type == MouseEventType.buttonPressed) {
        if (evt.button == MouseButton.left) {
          willy.setXY(evt.x/2, evt.y/2);
        }
      }
    },
    // char
    delegate (dchar ch) {
      if (sdwin.closed) return;
    },
  );
  x11gfxDeinit();
}
