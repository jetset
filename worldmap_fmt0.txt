room format:
  db "JSWROOM0"     ; signature
  dw roomCount      ; number of rooms in this file
  dd roomCount*(?)  ; offset in file for each room
  dw startRoom
  db startX, startY ; Willy's starting coordinates (in pixels, unsigned)
  db startDir       ; 0xff: left; 1: right
  dw winItemCount   ; how many items we should collect to win the game

room data follows -- for each room:
  db 32*16*(?)      ; room tiles
  ubyte flags       ; room flags
  ubyte srtype      ; special room type (see below)
  dw exitLeft       ; room index or -1
  dw exitUp         ; room index or -1
  dw exitRight      ; room index or -1
  dw exitDown       ; room index or -1
  db nameLen        ; chars in name
  db nameLen*(?)    ; name
  db len,len*(?)    ; willy's sprite lump
  dw wspr           ; willy's sprite
  db border         ; border color
  db tileCount      ; number of non-empty tiles in this room (usually 8)
  tile graphics info, repeats tileCount times
    db len,len*(?)    ; sprite lump
    dw spridx         ; index (unsigned)
    db ink, paper
  db monsterCount   ; number of monsters in this room
  monster info, repeats monsterCount times
    db len,len*(?)    ; sprite lump
    dw spridx         ; index (unsigned)
    db color          ; monster color
    db flags          ; monster flags
    db x, y           ; unsigned
    db dx, dy         ; signed
    db counter        ; dunno
    db reload         ; dunno
    db ofsmask        ; dunno

srtype:
  enum {
    Normal        = 0,
    MasterBedroom = 1, // 0x21
    Rocket        = 2, // 0x77
    RigorMortis   = 3, // 0x4b
    Crypt         = 4, // 0x4c
    HellTribbles  = 5, // 0x3b, 0x71: Highway to Hell, The Trubble with Tribbles
    Eggoids       = 6, // 0x7e
    Foot          = 7, // 0x67
    BeamDown      = 8, // 0x60
    BeamUp        = 9, // 0x7f
    Isle          = 10, // 0x50
    BellInvasion  = 11, // 0x44, 0x79: Belfry, Galactic Invasion
    TripSwitch    = 12, // 0x47
    Yacht         = 13, // 0x38
  }


note that switch in TripSwitch is reversed: it is initially right, but drawn as left
