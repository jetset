/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 * Based on Jet-Set Willy, v1.0.1 by <Florent.Guillaume@ens.fr>
 * Linux port and preliminary sound by jmd@dcs.ed.ac.uk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module engine;

import x11gfx;
import wadarc;

import willy : Willy; // circular dependency; sorry...


// ////////////////////////////////////////////////////////////////////////// //
struct WillyStart {
  int x, y;
  ushort room;
  int dir; // <0: left; >0: right
}

// ////////////////////////////////////////////////////////////////////////// //
__gshared ubyte[][string] gameLumps;
__gshared JSWRoom[] gameRooms;
__gshared int gameItemsLeftToWin; // how many items should left to "disarm" Maria
__gshared WillyStart willyStart;
__gshared ubyte[(JSWRoom.Width*8)*(JSWRoom.Height*8)] monmap; // !0: monster is there
__gshared uint frameno;


int countAllItems () {
  int res = 0;
  foreach (JSWRoom room; gameRooms) if (room !is null) res += room.countItems;
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
private void loadRooms (const(ubyte)[] data) {
  auto odata = data;
  gameRooms.length = 0;
  // sign
  if (data.length < 8) throw new Exception("invalid worldmap");
  if (data[0..8] != "JSWROOM1") throw new Exception("invalid worldmap");
  data = data[8..$];
  // room count
  if (data.length < 2) throw new Exception("invalid worldmap");
  uint roomCount = data[0]|(data[1]<<8);
  data = data[2..$];
  if (roomCount == 0) throw new Exception("invalid worldmap");
  // room offsets
  uint[] ofs;
  foreach (immutable _; 0..roomCount) {
    if (data.length < 4) throw new Exception("invalid worldmap");
    ofs ~= data[0]|(data[1]<<8)|(data[2]<<16)|(data[3]<<24);
    data = data[4..$];
  }
  // starting room
  if (data.length < 2) throw new Exception("invalid worldmap");
  willyStart.room = cast(ushort)(data[0]|(data[1]<<8));
  data = data[2..$];
  // starting x
  if (data.length < 1) throw new Exception("invalid worldmap");
  willyStart.x = data[0];
  data = data[1..$];
  // starting y
  if (data.length < 1) throw new Exception("invalid worldmap");
  willyStart.y = data[0];
  data = data[1..$];
  // starting dir
  if (data.length < 1) throw new Exception("invalid worldmap");
  willyStart.dir = (data[0]&0x80 ? -1 : 1);
  data = data[1..$];
  // items to win
  if (data.length < 2) throw new Exception("invalid worldmap");
  int iwin = cast(ushort)(data[0]|(data[1]<<8));
  data = data[2..$];
  // load rooms
  foreach (immutable ridx, uint rofs; ofs[]) {
    if (rofs == 0) { gameRooms ~= null; continue; }
    auto room = new JSWRoom();
    room.load(odata[rofs..$]);
    room.idx = cast(int)ridx;
    iwin -= room.countItems();
    gameRooms ~= room;
  }
  if (iwin < 0) iwin = 0;
  gameItemsLeftToWin = iwin;
}


public void loadGameData () {
  forEachWadFile(delegate (string name, uint size) {
    auto buf = wadLoadFile(name);
    if (name == "worldmap") {
      if (gameRooms.length) throw new Exception("too many worldmaps");
      loadRooms(buf);
      return;
    }
    gameLumps[name] = buf;
  });
  if (gameRooms.length == 0) throw new Exception("no worldmap");
  // ok, unvisited rooms can have any garbage in 'em
  /*
  foreach (JSWRoom room; gameRooms) {
    import std.stdio;
    writeln("loading room #", room.idx);
    room.loadRoomGfx();
  }
  */
}


// ////////////////////////////////////////////////////////////////////////// //
struct Rope {
  enum NBP = 31;
  enum RopePixelCount = 86;

  static immutable byte[RopePixelCount] ropeXOfs = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2,
  ];

  static immutable byte[RopePixelCount] ropeYOfs = [
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    2, 3, 3, 2, 3, 2, 3, 2, 3, 2, 2, 2, 3, 2, 2, 2,
    2, 2, 1, 2, 2, 1, 1, 2, 1, 1, 2, 2, 3, 2, 3, 2,
    3, 3, 3, 3, 3, 3,
  ];

  int ropeDir = 1;
  int ropeSpeed = -1;
  int ropePos = 26;

  void reset () {
    ropeDir = 1;
    ropeSpeed = -1;
    ropePos = 0x1a;
  }

  // return !0 from delegate to stop
  int forEachPoint (scope int delegate (int pxnum, int x, int y) dg) {
    if (ropePos < 0 || ropePos >= RopePixelCount/2 || dg is null) return 0;

    int x = 31*4+3;
    int y = 0;
    auto dx = &ropeXOfs[2*ropePos];
    auto dy = &ropeYOfs[2*ropePos];

    foreach (immutable i; 0..NBP) {
      if (auto res = dg(cast(int)i, x, y)) return res;
      if (ropeDir == 1) x += *dx++; else x -= *dx++;
      y += *dy++;
    }
    if (auto res = dg(NBP, x, y)) return res;
    return 0;
  }

  void draw () {
    //auto cc = palcol(gameData["sprmons"], 0x0b);
    forEachPoint(delegate (int pxnum, int x, int y) {
      if (pxnum >= NBP) return 0;
      setPixel(x, y, rgbcol(255, 255, 255));
      return 0;
    });
  }

  void step () {
    ropePos += (ropePos < 0x0c ? 2*ropeSpeed : ropeSpeed);
    if (ropePos < 0) {
      ropePos = 4;
      ropeDir = -ropeDir;
      ropeSpeed = -ropeSpeed;
    } else if (ropePos >= 0x1a) {
      ropeSpeed = -ropeSpeed;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class JSWRoom {
  enum Width = 32;
  enum Height = 16;

  // special room type
  enum SpType {
    Normal        = 0,
    MasterBedroom = 1, // 0x21
    Rocket        = 2, // 0x77
    RigorMortis   = 3, // 0x4b
    Crypt         = 4, // 0x4c
    HellTribbles  = 5, // 0x3b, 0x71: Highway to Hell, The Trubble with Tribbles
    Eggoids       = 6, // 0x7e
    Foot          = 7, // 0x67
    BeamDown      = 8, // 0x60
    BeamUp        = 9, // 0x7f
    Isle          = 10, // 0x50
    BellInvasion  = 11, // 0x44, 0x79: Belfry, Galactic Invasion
    TripSwitch    = 12, // 0x47
    Yacht         = 13, // 0x38
  }

  enum Tile {
    Nothing = 0,
    Crumb = 1, // wallsoft
    Wall = 2, // wallhard
    Deadly = 3, // die
    StairRight = 4,
    ConvLeft = 5, // moveleft
    Item = 6,
    StairLeft = 7,
    ConvRight = 8, // moveright
    SwitchCL = 9, // renders as ConvLeft (switch at left)
    SwitchCR = 10, // renders as ConvRight (switch at right)
  }

  enum Exit {
    Left = 0,
    Up = 1,
    Right = 2,
    Down = 3,
  }

  enum Flag {
    Rope = 1U<<0,
    NoAutomove = 1U<<1,
    DblAutomove = 1U<<2,
    Special = 1U<<3,
  }

  /*
  enum WillySpr {
    Normal = 6,
    Cosmonaut = 0xb7,
    Pig = 0x62,
  }
  */

  static struct Monster {
    enum Flag {
      Arrow = 1U<<0,
      OffIsX = 1U<<1,
      FixedDir = 1U<<2,
      OneWay = 1U<<3,
      Lift = 1U<<4,
      Friendly = 1U<<5,
    }
    string lump; // 8 chars; sprite lump
    const(ubyte)[] lmp; // lump data
    ushort spridx; // in lump
    int curspr; // can be negative
    int counter;
    int reload;
    int x;
    int y;
    int dx;
    int dy;
    ubyte ink;
    ubyte flags;
    ubyte ofsmask;

  public nothrow @safe @nogc:
    @property bool visible () const pure { pragma(inline, true); return (spridx != 0xffff); }
    void hide () { pragma(inline, true); spridx = 0xffff; }

    void move () {
      if (!visible) { curspr = -1; return; }
      int spr, offspr;
      auto flags = flags;
      if (flags&JSWRoom.Monster.Flag.Arrow) {
        x = (x+dx)&0xff;
        auto visible = (x < 0x20);
        spr = (visible ? spridx : -1);
      } else {
        if (--counter == 0) {
          counter = reload;
          dx = -dx;
          dy = -dy;
        }
        counter &= 0xff;
        x += dx;
        y += dy;
        spr = spridx;
      }
      offspr = (flags&JSWRoom.Monster.Flag.OffIsX ? x*(arrow ? 4 : 1) : counter);
      offspr &= ofsmask; // <=3
      if ((flags&JSWRoom.Monster.Flag.FixedDir) == 0) {
        if (flags&JSWRoom.Monster.Flag.OffIsX) {
          if (dx >= 0) offspr += 4;
        } else {
          if (dy < 0) offspr += 4;
        }
      }
      if (flags&JSWRoom.Monster.Flag.OneWay) {
        if (offspr >= 4) spr = -1;
      }
      if (spr != -1) spr += offspr;
      curspr = spr;
    }

    @property const pure {
      int scrx () { pragma(inline, true); return (((x*(arrow ? 4 : 1))&~3)<<1)&0xff; }
      int scry () { pragma(inline, true); return (y&0xff); }

      bool arrow () { pragma(inline, true); return ((flags&Flag.Arrow) != 0); }
      bool offIsX () { pragma(inline, true); return ((flags&Flag.OffIsX) != 0); }
      bool fixedDir () { pragma(inline, true); return ((flags&Flag.FixedDir) != 0); }
      bool oneWay () { pragma(inline, true); return ((flags&Flag.OneWay) != 0); }
      bool lift () { pragma(inline, true); return ((flags&Flag.Lift) != 0); }
      bool friendly () { pragma(inline, true); return ((flags&Flag.Friendly) != 0); }
      bool hostile () { pragma(inline, true); return ((flags&Flag.Friendly) == 0); }
    }
  }

  static struct TileGfx {
    string lump;
    const(ubyte)[] lmp; // lump data
    ushort spridx;
    ubyte ink, paper;
  }

  uint fileofs; // internal
  int idx; // room index (number); from 0
  string title;
  ubyte [Width*Height] map, origmap;
  ubyte flags; // room flags
  SpType sptype; // special room type
  string willyLump; // Willy's sprite lump for this room
  const(ubyte)[] willyLmp; // lump data
  ushort willySprIdx; // Willy's sprite for this room
  int[Exit.max+1] exits; // exits; -1: none
  ubyte border;
  Monster[] monsters, origmonsters;
  TileGfx[] tilegfx; // 0: empty, unused
  Rope rope;

public:
  @property bool hasRope () const pure nothrow @safe @nogc { pragma(inline, true); return ((flags&Flag.Rope) != 0) /*|| true*/; }

  void resetRoom () {
    map[] = origmap[];
    resetMonsters();
  }

  void resetMonsters () {
    monsters[] = origmonsters[];
    if (hasRope) rope.reset();
  }

  ubyte tileValueAt (int x, int y) {
    if (x >= 0 && y >= 0 && x < 4*Width && y < 8*Height) return map[(y/8*Width)+(x/4)];
    return Tile.Nothing;
  }

  Tile tileAt (int x, int y) {
    if (x >= 0 && y >= 0 && x < 4*Width && y < 8*Height) return cast(Tile)(map[(y/8*Width)+(x/4)]&0x0f);
    return Tile.Nothing;
  }

  void setTileAt (int x, int y, ubyte t) {
    if (x >= 0 && y >= 0 && x < 4*Width && y < 8*Height) map[(y/8*Width)+(x/4)] = t;
  }

  bool takeAt (int x, int y) {
    if (x >= 0 && y >= 0 && x < 4*Width && y < 8*Height) {
      if ((map[(y/8*Width)+(x/4)]&0x0f) == Tile.Item) {
        map[(y/8*Width)+(x/4)] = 0;
        return true;
      }
    }
    return false;
  }

  int countItems () {
    int res = 0;
    foreach (ubyte b; map[]) {
      if ((b&0x0f) == Tile.Item) ++res;
    }
    return res;
  }

  // ////////////////////////////////////////////////////////////////////// //
  void step () {
    // move monsters
    foreach (ref mon; monsters) mon.move();
    if (hasRope) rope.step();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void drawTiles () {
    loadRoomGfx();
    foreach (immutable y; 0..Height) {
      foreach (immutable x; 0..Width) {
        ubyte tn = map[y*Width+x];
        ubyte tt = tn&0x0f;
        if (tt == Tile.Nothing) continue;
        // replace special tiles
        if (tt == Tile.SwitchCL) tt = Tile.ConvLeft;
        if (tt == Tile.SwitchCR) tt = Tile.ConvRight;
        //
        auto tg = &tilegfx[tt];
        if (tt == Tile.Crumb) {
          drawTile(tg.lmp, tg.spridx, x*8, y*8, tg.ink, tg.paper, 0, tn>>4);
        } else if (tt == Tile.ConvRight) {
          drawTile(tg.lmp, tg.spridx, x*8, y*8, tg.ink, tg.paper, (frameno/1)&0x07+8, 0);
        } else if (tt == Tile.ConvLeft) {
          drawTile(tg.lmp, tg.spridx, x*8, y*8, tg.ink, tg.paper, (8-((frameno/1)&0x07))&0x07+8, 0);
        } else if (tt == Tile.Item) {
          drawTile(tg.lmp, tg.spridx, x*8, y*8, tg.ink, (tg.paper+x^y+frameno)&0x1f);
        } else {
          drawTile(tg.lmp, tg.spridx, x*8, y*8, tg.ink, tg.paper);
        }
      }
    }
    //drawStr((vbufW-strWidth(title))/2, JSWRoom.Height*8, title, rgbcol(255, 255, 0));
    drawStrProp((vbufW-strWidthProp(title))/2, JSWRoom.Height*8, title, rgbcol(255, 255, 0));
    if (hasRope) rope.draw();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void specInit (ref Willy willy) {
    switch (sptype) {
      case SpType.Foot:
        if (countItems() != 0) willy.foot_cnt = 0xbc;
        break;
      default:
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  void buildColMap (const ref Willy willy) {
    monmap[] = 0;
    foreach (const ref mon; monsters) {
      if (mon.curspr < 0) continue;
      if (mon.friendly || mon.lift) continue;
      putSpriteOnColMap(mon.lmp, mon.curspr, mon.scrx, mon.scry);
    }
    switch (sptype) {
      case SpType.Foot:
        auto lmp = gameLumps["sprmons"];
        if (countItems() == 0) {
          int y = 0xd0-willy.foot_cnt;
          putSpriteOnColMap(lmp, 0, 128, y/2);
          foreach (immutable fy; 16..y) putSpriteOnColMap(lmp, 0, 128, fy/2, 1);
        } else {
          putSpriteOnColMap(lmp, 0, 128, 16/2);
        }
        break;
      case SpType.BellInvasion:
        //{ import std.stdio; writeln("monster count: ", monsters.length); }
        foreach (immutable idx; 0..5) {
          if (idx >= monsters.length) break;
          int y = monsters[idx].y;
          int cnt = monsters[idx].dy;
          if (monsters[idx].counter == 1) cnt = -cnt;
          if (cnt < 0) {
            cnt = -cnt;
            y -= cnt;
          }
          //draw_thread
          int tx = monsters[idx].x;
          int ty = y;
          if (cnt) {
            foreach (immutable yy; 8..ty+1) {
              putPixelOnColMap(2*tx+16/2-1, yy);
            }
          }
        }
        break;
      default:
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  void drawMonsters (const ref Willy willy) {
    foreach (const ref mon; monsters) {
      if (mon.curspr < 0) continue;
      drawSprite(mon.lmp, mon.curspr, mon.scrx, mon.scry, mon.ink);
    }
    switch (sptype) {
      case SpType.Foot:
        auto lmp = gameLumps["sprmons"];
        if (/*willy.foot_cnt != 0 &&*/ countItems() == 0) {
          int y = 0xd0-willy.foot_cnt;
          drawSprite(lmp, 0, 128, y/2, 0);
          foreach (immutable fy; 16..y) drawSprite(lmp, 0, 128, fy/2, 0, 1);
        } else {
          drawSprite(lmp, 0, 128, 16/2, 0);
        }
        break;
      case SpType.BellInvasion:
        //{ import std.stdio; writeln("monster count: ", monsters.length); }
        foreach (immutable idx; 0..5) {
          if (idx >= monsters.length) break;
          int y = monsters[idx].y;
          int cnt = monsters[idx].dy;
          if (monsters[idx].counter == 1) cnt = -cnt;
          if (cnt < 0) {
            cnt = -cnt;
            y -= cnt;
          }
          //draw_thread
          int tx = monsters[idx].x;
          int ty = y;
          if (cnt) {
            foreach (immutable yy; 8..ty+1) {
              setPixel(2*tx+16/2-1, yy, palcol(monsters[idx].lmp, monsters[idx].ink));
            }
          }
        }
        break;
      default:
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  // it is safe to call this multiple times
  // this MUST be called at least once
  void loadRoomGfx () {
    // load willy
    if (auto lmp = willyLump in gameLumps) {
      willyLmp = *lmp;
    } else {
      throw new Exception("Willy lump '"~willyLump~"' not found");
    }
    // load tiles
    foreach (ref tg; tilegfx[1..$]) {
      if (auto lmp = tg.lump in gameLumps) {
        tg.lmp = *lmp;
      } else {
        throw new Exception("tile lump '"~tg.lump~"' not found");
      }
    }
    // load monsters
    foreach (immutable idx, ref mon; origmonsters[]) {
      if (auto lmp = mon.lump in gameLumps) {
        mon.lmp = *lmp;
        monsters[idx].lmp = mon.lmp;
      } else {
        throw new Exception("monster lump '"~mon.lump~"' not found");
      }
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  void stepSpecial (ref Willy willy) {
    switch (sptype) {
      case SpType.MasterBedroom:
        // first monster should be Maria
        if (monsters.length < 1) return; // no Maria
        if (!monsters[0].visible) return;
        if (countAllItems() > gameItemsLeftToWin) {
          monsters[0].x = (monsters[0].x^1)&0xfd;
          if (willy.y != 0x68) monsters[0].x |= (~(willy.y/8+1))&3;
        } else {
          // disarm Maria
          monsters[0].hide();
          /*
          if (!winning) {
            if (what_at(x, y+16) != I_MOVERIGHT) return 0;
            winning = 1;
          }
          toright = 1;
          toleft = 0;
          tojump = 0;
          */
        }
        break;
      case SpType.RigorMortis:
        if (monsters.length == 0 || !monsters[0].visible) break;
        if (monsters[0].dx == 0 && countItems() == 0) {
          monsters[0].dx = -1;
          monsters[0].counter = 0x1c;
          if (monsters.length > 1 && monsters[1].visible) {
            monsters[1].dx = 1;
            monsters[1].counter = 0x1c;
          }
        }
        break;
      case SpType.Crypt:
        // use SwitchCL tile type
        if (monsters.length == 0 || !monsters[0].visible) break;
        if (monsters[0].reload == 0x1f) break;
        if (tileAt(willy.x, willy.y) == Tile.SwitchCL || tileAt(willy.x+4, willy.y) == Tile.SwitchCL) {
          //xor_item(I_MOVELEFT, 128, 24, 0, 1);
          //xor_item(I_MOVERIGHT, 128, 24, 0, 1);
          if (tileAt(willy.x, willy.y) == Tile.SwitchCL) setTileAt(willy.x, willy.y, Tile.SwitchCR);
          if (tileAt(willy.x+4, willy.y) == Tile.SwitchCL) setTileAt(willy.x+4, willy.y, Tile.SwitchCR);
          monsters[0].reload += 0x10;
          if (monsters[0].dx < 0) monsters[0].counter += 0x10;
        }
        break;
      case SpType.Eggoids:
        foreach (immutable idx, ref mon; monsters) {
          if (((mon.y-4)&0xff) >= 0x60) mon.dy = -mon.dy;
          if (((mon.x-2)&0xff) >= 0x77) mon.dx = -mon.dx;
          //mon.counter = 0; //k8: this doesn't work!
          //if (idx == 0) { import std.stdio; writeln("egg: counter=", mon.counter, "; reload=", mon.reload); }
        }
        break;
      case SpType.Foot:
        if (willy.foot_cnt != 0 && countItems() == 0) {
          willy.foot_cnt -= 2;
          // this should probably build leg length, but i hacked it around
          /*
          int y = 0xd0-willy.foot_cnt;
          //put_sprite_noxor (0x000, 2*128, y, 3);
          int yy = 2+((y>>4)&0x0f);
          map[yy*Width+16] = Tile.Deadly;
          if (y >= 0xc0) map[(yy-1)*Width+16] = Tile.Deadly;
          */
        }
        break;
      /*
      case SpType.BellInvasion:
        foreach (immutable idx; 0..5) {
          if (idx >= monsters.length) break;
          int y = monsters[idx].y;
          int cnt = monsters[idx].dy;
          if (monsters[idx].counter == 1) cnt = -cnt;
          if (cnt < 0) {
            cnt = -cnt;
            y -= cnt;
          }
          draw_thread(monsters[i].x, y, cnt);
        }
        break;
        */
      default:
    }
  }

  static void cheatCollectAllItems () {
    foreach (JSWRoom room; gameRooms) {
      foreach (ref ubyte t; room.map) {
        if (t == Tile.Item) t = Tile.Nothing;
      }
    }
  }

private:
  // ////////////////////////////////////////////////////////////////////// //
  // see "worldmap_fmt.txt"
  void load (const(ubyte)[] data) {
    void readBuf (void[] buf) {
      import core.stdc.string : memcpy;
      if (buf.length == 0) return;
      if (data.length < buf.length) throw new Exception("invalid room data");
      memcpy(buf.ptr, data.ptr, buf.length);
      data = data[buf.length..$];
    }

    byte readS8 () {
      if (data.length < 1) throw new Exception("invalid room data");
      byte res = cast(byte)data[0];
      data = data[1..$];
      return res;
    }

    ubyte readU8 () {
      if (data.length < 1) throw new Exception("invalid room data");
      ubyte res = data[0];
      data = data[1..$];
      return res;
    }

    int readX16 () {
      if (data.length < 2) throw new Exception("invalid room data");
      ushort res = cast(ushort)(data[0]|(data[1]<<8));
      data = data[2..$];
      return (res != ushort.max ? res : -1);
    }

    short readS16 () {
      if (data.length < 2) throw new Exception("invalid room data");
      short res = cast(short)(data[0]|(data[1]<<8));
      data = data[2..$];
      return res;
    }

    ushort readU16 () {
      if (data.length < 2) throw new Exception("invalid room data");
      ushort res = cast(ushort)(data[0]|(data[1]<<8));
      data = data[2..$];
      return res;
    }

    string readStr () {
      auto len = readU8();
      if (len == 0) return null;
      auto s = new char[](len);
      readBuf(s[]);
      return cast(string)s; // safe cast
    }

    //readBuf(origmap[]);
    // unpack map
    {
      int dpos = 0;
      while (dpos < origmap.length) {
        ubyte b = readU8();
        foreach (immutable _; 0..(b>>4)+1) {
          if (dpos >= origmap.length) throw new Exception("invalid room map");
          origmap[dpos++] = b&0x0f;
        }
      }
    }

    flags = readU8();
    ubyte spt = readU8;
    if (spt > SpType.max) throw new Exception("invalid room special");
    sptype = cast(SpType)spt;
    exits[Exit.Left] = readX16();
    exits[Exit.Up] = readX16();
    exits[Exit.Right] = readX16();
    exits[Exit.Down] = readX16();

    title = readStr();

    willyLump = readStr();
    willySprIdx = readU16();

    border = readU8();

    tilegfx.length = readU8()+1;
    if (tilegfx.length > 15) throw new Exception("too many tiles in room");
    auto tilelump = readStr();

    //{ import std.stdio; writeln("room: [", title, "]; ", tilegfx.length, " tiles"); }

    foreach (ref tg; tilegfx[1..$]) {
      tg.lump = tilelump; //readStr();
      tg.spridx = readU16();
      tg.ink = readU8();
      tg.paper = readU8();
      //{ import std.stdio; writeln("tile: [", tg.lump, "]"); }
    }

    origmonsters.length = readU8();
    if (origmonsters.length > 0) {
      auto monlump = readStr();
      foreach (ref mon; origmonsters) {
        mon.lump = monlump; //readStr();
        mon.spridx = readU16();
        mon.ink = readU8();
        mon.flags = readU8();
        mon.x = readU8();
        mon.y = readU8();
        mon.dx = readS8();
        mon.dy = readS8();
        mon.counter = readU8();
        mon.reload = readU8();
        mon.ofsmask = readU8();
        mon.curspr = mon.spridx;
      }
    }

    map[] = origmap[];
    monsters = origmonsters.dup;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
VColor palcol (const(ubyte)[] lmp, ubyte c) {
  //c += 2;
  return rgbcol(lmp[c*3+0], lmp[c*3+1], lmp[c*3+2]);
}


// ////////////////////////////////////////////////////////////////////////// //
private ubyte ror() (ubyte b, ubyte c) { pragma(inline, true); return cast(ubyte)((b>>c)|(b<<(8-c))); }

void drawTile (const(ubyte)[] lmp, uint ofs, int x, int y, ubyte ink, ubyte paper, int xofs=0, int yofs=0) {
  ofs = 768+ofs*8;
  xofs *= 2;
  foreach (immutable dy; 0..8) {
    ubyte b = (dy >= yofs ? lmp[ofs++] : 0);
    if (xofs && dy == 0) b = b.ror(xofs&0x07);
    if (xofs && dy == 2) b = b.ror((8-(xofs&0x07))&0x07);
    foreach (immutable dx; 0..8) {
      setPixel(x+dx, y+dy, (b&0x80 ? palcol(lmp, ink) : palcol(lmp, paper)));
      b <<= 1;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void putPixelOnColMap (int x, int y) {
  if (x >= 0 && y >= 0 && x < JSWRoom.Width*8 && y < JSWRoom.Height*8) {
    monmap[y*(JSWRoom.Width*8)+x] = 1;
  }
}


void putSpriteOnColMap (const(ubyte)[] lmp, uint ofs, int x, int y, int height=16) {
  if (height < 1) return;
  if (height > 16) height = 16;
  ofs = 768+ofs*(16*2);
  foreach (immutable dy; 0..height) {
    foreach (immutable xo; 0..2) {
      ubyte b = lmp[ofs++];
      foreach (immutable dx; 0..8) {
        if (b&0x80) {
          int xx = x+xo*8+dx;
          int yy = y+dy;
          if (xx >= 0 && yy >= 0 && xx < JSWRoom.Width*8 && yy < JSWRoom.Height*8) {
            monmap[yy*(JSWRoom.Width*8)+xx] = 1;
          }
        }
        b <<= 1;
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void drawSprite (const(ubyte)[] lmp, uint ofs, int x, int y, ubyte ink, int height=16) {
  if (height < 1) return;
  if (height > 16) height = 16;
  ofs = 768+ofs*(16*2);
  foreach (immutable dy; 0..height) {
    foreach (immutable xo; 0..2) {
      ubyte b = lmp[ofs++];
      foreach (immutable dx; 0..8) {
        if (b&0x80) {
          int xx = x+xo*8+dx;
          int yy = y+dy;
          setPixel(xx, yy, palcol(lmp, ink));
        }
        b <<= 1;
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
bool checkSpriteCollision (const(ubyte)[] lmp, uint ofs, int x, int y) {
  ofs = 768+ofs*(16*2);
  foreach (immutable dy; 0..16) {
    foreach (immutable xo; 0..2) {
      ubyte b = lmp[ofs++];
      foreach (immutable dx; 0..8) {
        if (b&0x80) {
          int xx = x+xo*8+dx;
          int yy = y+dy;
          if (xx >= 0 && yy >= 0 && xx < JSWRoom.Width*8 && yy < JSWRoom.Height*8) {
            if (monmap[yy*(JSWRoom.Width*8)+xx]) return true;
          }
        }
        b <<= 1;
      }
    }
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
bool checkSpritePixelCollision (const(ubyte)[] lmp, uint ofs, int x, int y, int px, int py) {
  ofs = 768+ofs*(16*2);
  foreach (immutable dy; 0..16) {
    foreach (immutable xo; 0..2) {
      ubyte b = lmp[ofs++];
      foreach (immutable dx; 0..8) {
        if (b&0x80) {
          int xx = x+xo*8+dx;
          int yy = y+dy;
          if (xx == px && yy == py) return true;
        }
        b <<= 1;
      }
    }
  }
  return false;
}
